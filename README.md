# emerio-test-anggular2

membuat aplikasi sederhana untuk melihat postingan dengan menggunakan framework anggular 12.2.2 dan bootstrap 4.0
serta komunikasi dengan servernya menggunakan http client dan menerapkan multiple router untuk routingnya  

untuk endpoint yang bisa diggunakan : 

#landing page 
http://localhost:4200/

#login
http://localhost:4200/login

#post content
http://localhost:4200/post/content

#detail content and comment
http://localhost:4200/post/detail/1/Leanne%20Graham

#profile
http://localhost:4200/post/profile
