import { Component, OnInit } from '@angular/core';
import { Router} from '@angular/router';
@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  public profileJson=JSON.parse(localStorage.getItem("user-active") || '{}'); 
  constructor(private router: Router) { }

  ngOnInit(): void {
  
  }
  goHome(){
    this.router.navigate(['/post/content']);
  }
}
