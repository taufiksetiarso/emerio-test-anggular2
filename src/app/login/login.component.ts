import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup } from '@angular/forms';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { AppSettings } from '../utils/api';
import { HttpClient } from '@angular/common/http';
import { User } from '../models/user';
import { Profile } from '../models/profile';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  myForm = this.fb.group({
    username: [''],
    password: ['']
  })
  users: User[] = [];

  constructor(private fb: FormBuilder, private http: HttpClient, private route: ActivatedRoute,
    private router: Router) {
  }

  ngOnInit(): void {
    this.myForm = this.fb.group({
      username: '',
      password: ''
    })
  }

  onPasswordChange() {
    if (this.password.value == this.username.value) {
      this.password.setErrors(null);
    } else {
      this.password.setErrors({ mismatch: true });
    }
  }
  get username(): AbstractControl {
    return this.myForm.controls['username'];
  }
  get password(): AbstractControl {
    return this.myForm.controls['password'];
  }

  onLogin() {
    if (this.password.hasError('mismatch')) return;
    return this.http.get<User[]>(AppSettings.API_ENDPOINT + '/users')
      .subscribe(
        (response) => {
          this.users = response;
          for (let user of this.users) {
            if (user.username === this.username.value) {
              let profile = new Profile(user.username, user.email, user.address?.street, user.phone);
              localStorage.setItem('user-active', JSON.stringify(profile));
              this.router.navigate(['/post/content']);
              break;
            }
          }

        },
        (error) => {
          console.error('Request failed with error')
          alert(error);
        },
        () => {
          console.log('Request completed')
        })
  }
}
