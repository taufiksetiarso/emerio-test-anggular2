import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AppSettings } from '../utils/api';
import { HttpClient } from '@angular/common/http';
import { Post } from '../models/post';
import { PostContaintVo } from '../models/post-content-vo';
import { User } from '../models/user';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Comment } from '../models/comment';
@Component({
  selector: 'app-post-content',
  templateUrl: './post-content.component.html',
  styleUrls: ['./post-content.component.css']
})
export class PostContentComponent implements OnInit {
  name: string = ''
  isDone = false;
  username: string | undefined
  posts: PostContaintVo[] = []
  page = 1;
  pageSize = 10;
  dataPostContent = new Map<string, Post[]>();
  @Input() post!: PostContaintVo;
  constructor(private http: HttpClient, private route: ActivatedRoute,
    private router: Router) {
    this.name = ''
  }

  ngOnInit(): void {
    this.name = JSON.parse(localStorage.getItem("user-active") || '{}').username;
    this.fetchPost()
  }
  fetchPost() {
    return this.http.get<Post[]>(AppSettings.API_ENDPOINT + '/posts')
      .subscribe(
        (response) => {
          response.forEach(post => {
            let countComment = 0;
            this.fetchPostComments(post.userId).subscribe(comments => {
              countComment = comments.length;
            });
            this.fetchDetailUsername(post.userId).subscribe(user => {
              this.posts.push(new PostContaintVo(post.id, user.name, post.title, countComment))
            });

          })
        },
        (error) => {
          console.error('Request failed with error')
          alert(error);
        },
        () => {
          console.log('Request completed')
        })
  }

  fetchDetailUsername(userId: number | undefined): Observable<User> {
    return this.http.get(AppSettings.API_ENDPOINT + '/users/' + userId).pipe(map((result: any) => result));
  }
  onDetail(userId: number | undefined, username: string | undefined) {
    this.router.navigate(['/post/detail', userId, username]);
  }
  fetchPostComments(postId: number | undefined): Observable<Comment[]> {
    return this.http.get(AppSettings.API_ENDPOINT + '/posts/' + postId + '/comments').pipe(map((result: any) => result));
  }
}
