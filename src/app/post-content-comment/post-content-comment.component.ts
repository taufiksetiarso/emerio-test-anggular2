import { Component, OnInit,Input } from '@angular/core';
import { Comment } from '../models/comment';
@Component({
  selector: 'app-post-content-comment',
  templateUrl: './post-content-comment.component.html',
  styleUrls: ['./post-content-comment.component.css']
})
export class PostContentCommentComponent implements OnInit {
  @Input() comment!: Comment;
  constructor() { }

  ngOnInit(): void {
  }

}
