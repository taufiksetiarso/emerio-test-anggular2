import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PostContentDetailComponent } from './post-content-detail.component';

describe('PostContentDetailComponent', () => {
  let component: PostContentDetailComponent;
  let fixture: ComponentFixture<PostContentDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PostContentDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PostContentDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
