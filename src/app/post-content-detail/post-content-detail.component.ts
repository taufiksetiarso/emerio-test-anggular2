import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AppSettings } from '../utils/api';
import { HttpClient } from '@angular/common/http';
import { Post } from '../models/post';
import { Comment } from '../models/comment';
import { Observable } from 'rxjs';
import { map} from 'rxjs/operators';
@Component({
  selector: 'app-post-content-detail',
  templateUrl: './post-content-detail.component.html',
  styleUrls: ['./post-content-detail.component.css']
})
export class PostContentDetailComponent implements OnInit {
  postVo: Post=new Post()
  postComments:Comment[]=[]
  username:string=""
  showComment:boolean=false
  constructor(private http: HttpClient,private route: ActivatedRoute,
    private router: Router) { }

  ngOnInit(): void {
    let postId=this.route.snapshot.paramMap.get('id');
    this.username=""+this.route.snapshot.paramMap.get('username')
    this.fetchPostDetail(Number(postId) ).subscribe(post=> {
        this.postVo=post
     });
     this.fetchPostComments(Number(postId) ).subscribe(comments=> {
      this.postComments=comments
   });
  }
  fetchPostDetail(postId:number|undefined): Observable<Post> {
    return this.http.get(AppSettings.API_ENDPOINT + '/posts/'+postId).pipe(map((result:any)=>result));
  }
  fetchPostComments(postId:number|undefined): Observable<Comment[]> {
    return this.http.get(AppSettings.API_ENDPOINT  + '/posts/'+postId+'/comments').pipe(map((result:any)=>result));
  }
  goHome(){
    this.router.navigate(['/post/content']);
  }
}
