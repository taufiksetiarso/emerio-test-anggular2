export class Profile {

  username: string | undefined;
  email: string | undefined;
  address: string | undefined;
  phone: string | undefined;
  constructor(username: string | undefined, email: string | undefined, address: string | undefined, phone: string | undefined) {
    this.username = username
    this.email = email
    this.address = address
    this.phone = phone
  }
}