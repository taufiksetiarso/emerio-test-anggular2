export class PostContaintVo {

  postId: number | undefined;
  userName: string | undefined;
  title: string | undefined;
  commentCount: number | undefined;
  constructor(postId: number | undefined, userName: string | undefined, title: string | undefined, commentCount: number | undefined) {
    this.postId = postId
    this.userName = userName
    this.title = title
    this.commentCount = commentCount
  }
}