export class User {


    id: number | undefined;
    name: string | undefined;
    username: string | undefined;
    email: string | undefined;
    address: {
        street: string | undefined;
        suite: string | undefined;
        city: string | undefined;
        zipcode: string | undefined;
        geo: {
            lat: string | undefined;
            lng: string | undefined;
        }
    } | undefined;
    phone: string | undefined;
    website: string | undefined;
    company: {
        name: string | undefined;
        catchPhrase: string | undefined;
        bs: string | undefined;
    } | undefined;
}