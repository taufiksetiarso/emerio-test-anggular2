import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PostContentDetailComponent } from '../post-content-detail/post-content-detail.component';
import { PostContentCommentComponent } from '../post-content-comment/post-content-comment.component';
import { PostContainerComponent } from '../post-container/post-container.component';
import { PostContentComponent } from '../post-content/post-content.component';
import { PostContainerRoutingModule } from './post-container-routing.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
@NgModule({
  declarations: [ PostContentComponent,
    PostContainerComponent,
    PostContentDetailComponent,
    PostContentCommentComponent],
  imports: [
    CommonModule,
    PostContainerRoutingModule,
    NgbModule,
    ReactiveFormsModule,
    HttpClientModule,
  ],
  providers: [],
  bootstrap: [PostContainerComponent]
})
export class PostContainerModule { }
