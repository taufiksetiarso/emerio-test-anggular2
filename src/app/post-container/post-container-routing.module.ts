import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PostContentDetailComponent } from '../post-content-detail/post-content-detail.component';
import { PostContentCommentComponent } from '../post-content-comment/post-content-comment.component';
import { PostContainerComponent } from '../post-container/post-container.component';
import { PostContentComponent } from '../post-content/post-content.component';
import { ProfileComponent } from '../profile/profile.component';
const routes: Routes = [
  {
    path: '', component: PostContainerComponent, children: [
      {
        path: 'detail/:id/:username', component: PostContentDetailComponent
      },
      {
        path: 'comment', component: PostContentCommentComponent
      },
      {
        path: 'content', component: PostContentComponent
      },
      {
        path: 'profile', component: ProfileComponent
      },

    ],
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PostContainerRoutingModule { }
